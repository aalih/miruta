###################
What is MiRuta
###################

MiRuta is a project of the Dirección General de Transporte (DGT) de La Habana to provide information about the
public transportation in La Habana.

This code correspond to the web frontend of the system.

*******************
Requirements
*******************

PHP version 5.4 or newer is recommended.

Postgres 9.x + PostGis extension

************
Installation
************

Create a Postgres database and load the file ../db/schema.sql
Create the files application/config/database.php and application/config/miruta.php with the values for
your environment, use the examples files in application/config as a template.

***************
Acknowledgement
***************

Created by `Transoft <http://www.transoft.transnet.cu/>` in collaboration with `Alfonso Ali <http://aalih.cubava.cu/>` from `GUTL <http://gutl.jovenclub.cu/>`
