

<div class="container banner_samll">
    <div class="row">
        
        <div class='col-md-6 banner_right_small'>
                <i class="flag_small"></i>
                <div class="flag_desc">
                    <h3>Paradas</h3>
                    <p>Listado de paradas actualizadas</p>
                 
                </div>
                <div class="clearfix"> </div>

        </div>
    </div>
</div>
<div class="main">
   
    
    
<div class='container content_middle'>
        <div class="row">
            
            <div class="sap_tabs">	
						 <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
						  <ul class="resp-tabs-list">
                              <?php foreach ($recorridos as $recorrido): ?>
                                  <li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span><?php echo $recorrido['ruta']. ' '.$recorrido['inicio'].' - '.$recorrido['final']; ?></span></li>
                              <?php endforeach; ?>
							  <div class="clear"></div>
                              <div class="clear"></div>
						  </ul>				  	 
							<div class="resp-tabs-container">
                                <?php foreach ($recorridos as $recorrido): ?>
                                    <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
                                        <div class="facts">
                                            <div class="tab_box">
                                                <h4><a href="#"><?php echo $recorrido['ruta']. ' '.$recorrido['inicio'].' - '.$recorrido['final']; ?></a></h4>
                                                <p>Listado de paradas actualizadas</p>
                                            </div>
                                            <ul class="tab_list">
                                                <li><a href="#">Parada 1</a></li>
                                                <li><a href="#">Parada 1</a></li>
                                                <li><a href="#">Parada 1</a></li>
                                                <li><a href="#">Parada 1</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                                       
							  </div>	
					        </div>
					  </div>
          
        </div>

</div>
