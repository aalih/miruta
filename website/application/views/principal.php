
<div class="container banner">
    <div class="row">
        <div class='col-md-4 banner_left'>
            <span></span>
        </div>
        <div class='col-md-6 banner_right'>
            <h1>siga su ruta </h1>
            <h2>nuestras aplicaciones lo guiarán</h2>
            <p> Aquí va una explicación sobre las bondades de la aplicación, la web y la móvil, para que sirve y como se usa...</p>
            <a class="banner_btn" href="">Más información</a>
        </div>
    </div>
</div>
<div class="main">
    <div class='container content_top'>
        

        <div class='row'>
            <div class="col-md-4 flag_grid">
                <i class="flag"></i>
                <div class="flag_desc">
                    <h3><a href="<?php echo base_url();?>paradas/index">Paradas</a></h3>
                    <p>Listado de paradas actualizadas</p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-4 flag_grid">
                <i class="camera"></i>
                <div class="flag_desc">
                    <h3><a href="#">Noticias</a></h3>
                    <p>Noticias actualizadas</p>
                    <a href="#" class="link">leer más</a>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-4">
                <i class="home"></i>
                <div class="flag_desc">
                    <h3><a href="#">Mapa</a></h3>
                    <p>Localización</p>
                    <a href="#" class="link">leer más</a>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
<div class='container content_middle'>
        <div class="row">
            <div class="col-md-8 middle_left">
                <ul class="test_box middle_grid">
                    <li class="test_img"><img src="<?php echo base_url();  ?>/assets/images/pic1.jpg" class="img-responsive" alt=""> </li>
                    <li class="test_desc">
                        <h4><a href="#">Cambio de rutas (Articulo)</a></h4>
                        <p>Articulo más reciente relacionado con una noticia en específico sobre cualquier información de transporte, rutas etc etc…</p>
                        <br><a class="content_btn" href="">Ver más</a>
                    </li>
                    <div class="clearfix">	</div>
                </ul>
               
            </div>
          
        </div>
    
    
    <div class="row">
            <div class="col-md-8 middle_left">
                <ul class="test_box middle_grid">
                    <li class="test_img"><img src="<?php echo base_url();  ?>/assets/images/pic1.jpg" class="img-responsive" alt=""> </li>
                    <li class="test_desc">
                        <h4><a href="#">Cambio de rutas (Articulo)</a></h4>
                        <p>Articulo más reciente relacionado con una noticia en específico sobre cualquier información de transporte, rutas etc etc…</p>
                        <br><a class="content_btn" href="">Ver más</a>
                    </li>
                    <div class="clearfix">	</div>
                </ul>
               
            </div>

        
          
        </div>
    
    
    
    
    
    
    
    
    </div> 
  

   
    
    <div class="container content_bottom">
      
        <p class="m_1">Nuestros usuarios opinan</p>
        <div class="row">
            <?php foreach ($array as $value): ?>
            <div class='col-md-6 comment_top'>
                <div class="project_grid">

                        <ul class="project_box">
                            <li class="mini-post-meta"><time datetime=""><span class="day"><?php echo $value['dia']; ?></span><span class="month"><?php echo $value['mes']; ?></span></time></li>
                            <li class="desc"><h5><a href="#"><?php echo $value['tipo']; ?></a></h5>
                                <span class="comment"><?php echo $value['count']; ?> Comentarios</span>
                                <p><?php echo $value['descripcion']; ?>...<a href="#"><img src="<?php echo base_url();  ?>/assets/images/comment_arrow.png" alt=""/></a></p>
                            </li>
                            <div class="clearfix"> </div>
                        </ul>

                </div>
            </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>