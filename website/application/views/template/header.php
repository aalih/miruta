<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    <title>MiRuta</title>
    <link href="<?php echo base_url();  ?>/assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url();  ?>/assets/css/font-awesome/css/font-awesome.css" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url();  ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();  ?>/assets/js/bootstrap.min.js"></script>
    <!-- Custom Theme files -->
    <link href="<?php echo base_url();  ?>/assets/css/style.css" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfont-->

    <script src="<?php echo base_url();  ?>/assets/js/easyResponsiveTabs.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true   // 100% fit in a container
            });
        });

        $(function(){

            $(document).on( 'scroll', function(){

                if ($(window).scrollTop() > 100) {
                    $('.scroll-top-wrapper').addClass('show');
                } else {
                    $('.scroll-top-wrapper').removeClass('show');
                }
            });

            $('.scroll-top-wrapper').on('click', scrollToTop);
        });

        function scrollToTop() {
            verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
            element = $('body');
            offset = element.offset();
            offsetTop = offset.top;
            $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
        }
    </script>
</head>
<body>
<div class="scroll-top-wrapper ">
	<span class="scroll-top-inner">
		<i class="fa fa-2x fa-arrow-circle-up"></i>
	</span>
</div>
<!----- start-header---->
<div class="wrapper">
<!----start-header---->
<div class="header">
    <div class="container header_top">
        <div class="logo">
            <img src="<?php echo base_url();  ?>/assets/images/logo.jpg" alt="">
        </div>
        <div class="menu">
            <a class="toggleMenu" href="#"><img src="<?php echo base_url();  ?>/assets/images/nav_icon.png" alt="" /> </a>
            <ul class="nav" id="nav">
                <?php
                    if($current == "inicio")
                        echo '<li class="current"><a href="">Inicio</a></li>' ;
                    else
                    {
                        echo '<li><a href="'.base_url().'principal/index">Inicio</a></li>';
                    }
                ?>
                <?php
                if($current == "paradas")
                    echo '<li class="current"><a href="">Paradas</a></li>' ;
                else
                {
                    echo '<li><a href="'.base_url().'paradas/index">Paradas</a></li>';
                }
                ?>
                <?php
                if($current == "noticias")
                    echo '<li class="current"><a href="">Noticias</a></li>' ;
                else
                {
                    echo '<li><a href="">Noticias</a></li>';
                }

                ?>
                <?php
                if($current == "mapa")
                    echo '<li class="current"><a href="">Mapa</a></li>' ;
                else
                {
                    echo '<li><a href="'.base_url().'mapa/index">Mapa</a></li>';
                }
                ?>
                <?php
                if($current == "galeria")
                    echo '<li class="current"><a href="">Galeria</a></li>' ;
                else
                {
                    echo '<li><a href="">Galeria</a></li>';
                }
                ?>
                <?php
                if($current == "contacto")
                    echo '<li class="current"><a href="">Contacto</a></li>' ;
                else
                {
                    echo '<li><a href="'.base_url().'contacto/index">Contacto</a></li>';
                }
                ?>
                <?php
                if($current == "comentario")
                    echo '<li class="current"><a href="">Comentario</a></li>' ;
                else
                {
                    echo '<li><a href="'.base_url().'comentario/index">Comentario</a></li>';
                }
                ?>


                <div class="clearfix"></div>
            </ul>
            <script type="text/javascript" src="<?php echo base_url();  ?>/assets/js/responsive-nav.js"></script>
        </div>
        <div class="clearfix"> </div>
        <!----//End-top-nav---->
    </div>
</div>
<!----- //End-header---->
