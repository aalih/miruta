

<div class="container banner_samll">
    <div class="row">
        <div class='col-md-4 banner_right_small'>
            <div class="clearfix"> </div>
	    <div id="autocomplete_container">
		<label>Paradas:</label>
		<input id="paradas">
	    </div>
           
        </div>
        <div class='col-md-7'>
           <div>
              <select name="base" id="bases-select">
                <?php foreach ($bases as $base) { ?>
                  <option value="<?= $base['id'] ?>"<?php if ($base['id'] == $bid) {?> selected <?php } ?>><?= $base['nombre'] ?></option>
                <?php } ?>
	      </select>
              <select name="rutas" id="recorridos-select">
	        <option value="0">--------------------seleccione un recorrido a mostrar--------------------</option>
	        <?php foreach ($recorridos as $recorrido) { ?>
	          <option value="<?= $recorrido['id'] ?>"><?= $recorrido['ruta'].' '.$recorrido['inicio'].' - '.$recorrido['final'] ?></option>
	        <?php } ?>
	      </select>
            </div>
        </div>
    </div>
</div>
<div class="main">
    <link href="<?= base_url() ?>assets/css/jquery-ui.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/leaflet/leaflet.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/leaflet/Control.Loading.css" rel="stylesheet">
    <div class='container' style="padding-left:0; padding-right:0; position:relative;">
	<div id="map" style="height: 700px"></map>
        <div id="control-recorridos">
              <button href="#" class="dec">Anterior</button>
              <button href="#" class="inc">Siguiente</button>
        </div>
    </div>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/leaflet/leaflet.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/leaflet/Control.Loading.js"></script>
    <script type="text/javascript">
       var map = L.map('map', {loadingControl: true}).setView([23.12248, -82.3798], 15);

       L.tileLayer('<?= $tile_url ?>', {
          maxZoom: 17,
	  minZoom: 14
       }).addTo(map);

       var paradas = {};

       map.fire('dataloading');
       $.getJSON('<?= base_url() ?>mapa/paradas', function(data) {
	    map.fire('dataload');
	    L.geoJson(data.paradas, {
		onEachFeature: function(feature, layer) {  
		    layer.bindPopup('Cargando...');
		    layer.on('popupopen', function(evt) {
			var p = evt.target.getPopup();
			$.get('<?= base_url() ?>mapa/parada?id=' + feature.properties.id).done(function(d) {
			    p.setContent(d);
			    p.update();
			});
		    });
		    paradas[feature.properties.id] = layer;
		}
	    }).addTo(map);
	});

       var $r_control = $('#control-recorridos button');
       var recorrido = undefined;
       var r_paradas = undefined;
       var r_pos = 0;
       
       $('#bases-select').on('change', function(evt) {
	    var base_id = $(this).val();
            window.location.href = '<?= base_url() ?>mapa/?bid=' + base_id;
       });

       $('#recorridos-select').on('change', function(evt) {
         var rec_id = $(this).val();
         if (rec_id != '0') {
           $r_control.hide()
           map.fire('dataloading');
           $.getJSON('<?= base_url() ?>mapa/recorrido', {id: rec_id}, function(data) {
              map.fire('dataload');

              if (recorrido) {
                map.removeLayer(recorrido);
              }
              if (data.recorrido != "") {
                recorrido = L.geoJson(data.recorrido, {
                  style: function(feature) {
                    return {color: '#000'};
                  }
                }).addTo(map);
              }
              var bounds = recorrido.getBounds();
              map.panInsideBounds(bounds);
	      r_pos = 0;
	      r_paradas = data.paradas;
	      $r_control.show()
	   });  
         }
         else {
	    $r_control.hide();
            map.removeLayer(recorrido);
	    recorrido = undefined;
	 }
       });

       $('#paradas').autocomplete({
	  source: "<?= base_url() ?>paradas/search?",
	  select: function(e, ui) {
	     e.preventDefault();
             marker = paradas[ui.item.value];
	     map.panTo(marker.getLatLng());
	     marker.openPopup();	     
	  }
       });
       
       $r_control.on('click', function(evt) {
	    evt.preventDefault();
	    evt.stopPropagation();
	    pos = $(this).hasClass('inc') ? r_pos + 1 : r_pos -1;
	    if (pos < 0) {
               pos = 0;
	    } 
	    if (pos >= r_paradas.length) {
	       pos = r_paradas.length - 1
	    }
	    r_pos = pos;
            id = r_paradas[r_pos];
	    console.log(id);
	    marker = paradas[id];
	    map.panTo(marker.getLatLng());
	    marker.openPopup();
       });
           </script>
     
</div>
