<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comentario extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('comentarios_model');
        $this->load->library('session');
        //$this->config->set_item('language','english');
    }

    public function index()
    {

        $dataheader['current'] = "comentario";
        $data['tipocomentario'] = $this->comentarios_model->obtener_tipos_comentarios();
        if (isset($_SESSION['msg']))
        {
            $data['msg'] = $_SESSION['msg'];
            $data['res'] = $_SESSION['res'];
        }
        $data['val'] = false;

        $this->load->helper('form');
        $this->load->view('template/header',$dataheader);
        $this->load->view('comentario',$data);
        $this->load->view('template/footer');

    }

    public function send()
    {

        $dataheader['current'] = "comentario";
        $this->load->library('form_validation');

        $tipo = $this->input->post('combotipo');
        $nombre = $this->input->post('nombre');
        $mensaje = $this->input->post('mensaje');

        $fechaactual = date('Y-m-d');

        $data = array(
            'usuario' => $nombre,
            'descripcion' => $mensaje,
            'fecha' => $fechaactual,
            'miruta_tipo_comentarioidtipo' => $tipo,
            'aprobado' => false
        );

        $this->form_validation->set_rules('nombre', 'Nombre', 'required');
        $this->form_validation->set_rules('mensaje', 'Mensaje', 'required');

        if ($this->form_validation->run() === FALSE) {
            $data['val'] = true;
            $data['tipocomentario'] = $this->comentarios_model->obtener_tipos_comentarios();
            $this->load->view('template/header',$dataheader);
            $this->load->view('comentario',$data);
            $this->load->view('template/footer');
        } else {


                    $result = $this->comentarios_model->insertar_comentario($data);

                if ($result['success'] === true) {
                    $_SESSION['res'] = 'success';
                    $_SESSION['msg'] = 'Se ha insertado satisfactoriamente el comentario';
                } else
                {
                    $_SESSION['res'] = 'failure';
                    $_SESSION['msg'] = 'Error insertando el comentario';
                }

                $this->session->mark_as_flash('msg');
                $this->session->mark_as_flash('res');
                redirect('comentario/index');

        }


    }
}