<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller {



    public function index()
    {
        $dataheader['current'] = "contacto";

        $this->load->view('template/header',$dataheader);
        $this->load->view('contacto');
        $this->load->view('template/footer');
    }
}