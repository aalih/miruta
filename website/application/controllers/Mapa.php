<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapa extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('recorridos_model');
	$this->load->model('paradas_model');
    }

    public function index()
    {
        $base_id = array_key_exists('bid', $_GET) ? $_GET['bid'] : null;
	$data['bases'] = $this->recorridos_model->getBases();
        if (count($data['bases']) > 0) {
          $base_id = $base_id ? $base_id : $data['bases'][0]['id'];
          $data['recorridos'] = $this->recorridos_model->getRecorridos($base_id);
        }
        else {
          $data['recorridos'] = array();
        }
        $data['bid'] = $base_id;
	$data['tile_url'] = $this->config->item('tile_url');
        $dataheader['current'] = "recorridos";

        $this->load->view('template/header',$dataheader);
        $this->load->view('mapa', $data);
        $this->load->view('template/footer');
    }

    public function paradas() {
	$data['paradas'] = array();
        foreach($this->paradas_model->getParadas() as $parada) {
	    $elem = '{"type": "Feature", "geometry": '.$parada['geometry'].', "properties": {"id": '.$parada['id'].'}}';
	    array_push($data['paradas'], $elem);
	}
	$this->load->view('paradas_data', $data);
    }

    public function parada() {
	$data['parada'] = $this->paradas_model->getParada($_GET['id']);
        $data['rutas'] = $this->paradas_model->getRecorridos($_GET['id']);
	$this->load->view('popup_parada', $data);
    }

    public function recorrido() {
	$data['recorrido'] = $this->recorridos_model->getRecorrido($_GET['id']);

        $func = function($value) { 
	    return $value['id'];
	};
        $data['paradas'] = array_map($func, $this->recorridos_model->getParadas($_GET['id']));

	$this->load->view('recorrido_data', $data);
    }
}
