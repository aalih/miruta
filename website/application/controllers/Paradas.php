<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paradas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('recorridos_model');
	$this->load->model('paradas_model');
    }

    public function index()
    {

        $data['recorridos'] = $this->recorridos_model->getRecorridos();
        $dataheader['current'] = "paradas";

        $this->load->view('template/header',$dataheader);
        $this->load->view('paradas',$data);
        $this->load->view('template/footer');
    }

    public function search()
    {
	$data['paradas'] = array();
	foreach ($this->paradas_model->searchByAddress($_GET['term']) as $parada) {
	    $elem = '{"value": "'.$parada['id'].'", "label": "'.$parada['direccion'].'"}';
	    array_push($data['paradas'], $elem);
	}
	$this->load->view('paradas_search', $data);
    }
}
