<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller {

private $meses = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");
    public function __construct()
    {
        parent::__construct();
        //$this->config->set_item('language','english');
        $this->load->model('paradas_model');
        $this->load->model('comentarios_model');

    }

    public function index()
    {
        $dataheader['current'] = "inicio";

        $tipo_comentarios =$this->comentarios_model->obtener_tipos_comentarios();
        $array = array();

        for($i = 0; $i < count($tipo_comentarios);$i++)
        {
            $array[$i]['tipo']=$tipo_comentarios[$i]["descripcion"];
            $array[$i]['count']=$this->comentarios_model->countComentarios($tipo_comentarios[$i]["idtipo"]);

            $res = $this->comentarios_model->lastComentarios($tipo_comentarios[$i]["idtipo"]);
            $array[$i]['descripcion']=$res["descripcion"];
            ((int)((substr($res["fecha"], 5, 2)-1) > 0 ? $array[$i]['mes']= $this->meses[(substr($res["fecha"], 5, 2)-1)] : $array[$i]['mes']=$this->meses[(int)date('m')-1]));
            $array[$i]['dia']=substr($res["fecha"], 8, 2);


        }
        $data["array"]=$array;
        $this->load->view('template/header',$dataheader);
        $this->load->view('principal',$data);
        $this->load->view('template/footer');
    }
}
