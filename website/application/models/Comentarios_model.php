<?php

class Comentarios_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    /************************Read*************************************************/
    public function obtener_tipos_comentarios()
    {
        $this->db->select("com.*");

        $query = $this->db->get('public.miruta_tipo_comentario as com');
        return $query->result_array();

    }

    public function insertar_comentario($comentario)
    {
        $query = $this->db->insert('public.miruta_comentario', $comentario);
        if ($query){
            return array('success'=>true);
        }else{
            return array('success'=>false, 'error'=>'NO_INSERTAR');
        }
    }

    public function countComentarios($id)
    {
        $this->db->select('idcomentario');
        $this->db->from('miruta_comentario');
        $this->db->where('miruta_tipo_comentarioidtipo', $id);
        $this->db->where('aprobado', 't');
        $query = $this->db->get();
        return count($query->result_array());

    }

    public function lastComentarios($id)
    {

        $this->db->select_max('idcomentario');
        $this->db->from('miruta_comentario');
        $this->db->where('miruta_tipo_comentarioidtipo', $id);
        $this->db->where('aprobado', 't');
        $query_max = $this->db->get();
        $result = $query_max->row_array();


        $this->db->select('idcomentario,usuario,descripcion,fecha');
        $this->db->from('miruta_comentario');
        $this->db->where('idcomentario', $result['idcomentario']);
        $query = $this->db->get();
        return $query->row_array();

    }




    }