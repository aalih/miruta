<?php

class Paradas_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function getParadas()
    {
	$this->db->select('id, direccion, ST_AsGeoJSON(geom) as geometry');
	$this->db->from('miruta_punto');
	$query = $this->db->get();
	return $query->result_array();	
    }

    public function getParada($id) {
	$this->db->select('direccion');
	$this->db->from('miruta_punto');
	$this->db->where('id', $id);
	$query = $this->db->get();
	return $query->row_array();
    }

    public function getRecorridos($id) {
	$this->db->select('nombre, inicio, final');
	$this->db->from('miruta_recorrido');
	$this->db->join('miruta_parada', 'miruta_recorrido.id=miruta_parada.recorrido_id');
	$this->db->join('miruta_ruta', 'miruta_ruta.id=miruta_recorrido.ruta_id');
	$this->db->where('miruta_parada.punto_id', $id);
	$query = $this->db->get();
	return $query->result_array();
    }

    public function searchByAddress($term) {
	$this->db->select('id, direccion');
	$this->db->from('miruta_punto');
	$this->db->like('miruta_punto.direccion', $term);
	$query = $this->db->get();
	return $query->result_array();
    }
}
