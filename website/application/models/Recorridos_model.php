<?php

class Recorridos_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function getRecorridos($base_id)
    {
        $this->db->select('a.id, b.nombre as ruta, a.inicio, a.final');
        $this->db->from('miruta_recorrido as a, miruta_ruta as b');
        $this->db->where('b.id = a.ruta_id');
        $this->db->where('b.base_id', $base_id);
        $this->db->order_by('b.nombre, a.inicio, a.final','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getRecorrido($id)
    {
	$this->db->select('descripcion, ST_AsGeoJSON(geom) as geometry');
	$this->db->from('miruta_recorrido');
	$this->db->where('id', $id);
	$query = $this->db->get();
	return $query->row_array();
    }

    public function getParadas($id)
    {
	$this->db->select('miruta_punto.id');
	$this->db->from('miruta_punto');
	$this->db->join('miruta_parada', 'miruta_punto.id = miruta_parada.punto_id');
	$this->db->where('miruta_parada.recorrido_id', $id);
	$this->db->order_by('miruta_parada.posicion');
	$query = $this->db->get();
	return $query->result_array();
    }

    public function getBases() {
	$this->db->select('id, nombre');
        $this->db->from('miruta_base');
        $this->db->order_by('nombre');
        $query = $this->db->get();
        return $query->result_array();
    }
}
