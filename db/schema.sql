--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: miruta_base; Type: TABLE; Schema: public; Owner: miruta; Tablespace: 
--

CREATE TABLE miruta_base (
    id integer NOT NULL,
    nombre character varying(255) NOT NULL,
    direccion character varying(255),
    telefono character varying(25),
    correo character varying(50)
);


ALTER TABLE miruta_base OWNER TO miruta;

--
-- Name: miruta_base_id_seq; Type: SEQUENCE; Schema: public; Owner: miruta
--

CREATE SEQUENCE miruta_base_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE miruta_base_id_seq OWNER TO miruta;

--
-- Name: miruta_base_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: miruta
--

ALTER SEQUENCE miruta_base_id_seq OWNED BY miruta_base.id;


--
-- Name: miruta_grupo; Type: TABLE; Schema: public; Owner: miruta; Tablespace: 
--

CREATE TABLE miruta_grupo (
    id integer NOT NULL,
    codigo character varying(255)
);


ALTER TABLE miruta_grupo OWNER TO miruta;

--
-- Name: miruta_grupo_id_seq; Type: SEQUENCE; Schema: public; Owner: miruta
--

CREATE SEQUENCE miruta_grupo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE miruta_grupo_id_seq OWNER TO miruta;

--
-- Name: miruta_grupo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: miruta
--

ALTER SEQUENCE miruta_grupo_id_seq OWNED BY miruta_grupo.id;


--
-- Name: miruta_noticia; Type: TABLE; Schema: public; Owner: miruta; Tablespace: 
--

CREATE TABLE miruta_noticia (
    id integer NOT NULL,
    usuario character varying(255),
    descripcion character varying(255),
    fecha date NOT NULL,
    imagen text NOT NULL
);


ALTER TABLE miruta_noticia OWNER TO miruta;

--
-- Name: miruta_noticia_id_seq; Type: SEQUENCE; Schema: public; Owner: miruta
--

CREATE SEQUENCE miruta_noticia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE miruta_noticia_id_seq OWNER TO miruta;

--
-- Name: miruta_noticia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: miruta
--

ALTER SEQUENCE miruta_noticia_id_seq OWNED BY miruta_noticia.id;


--
-- Name: miruta_parada; Type: TABLE; Schema: public; Owner: miruta; Tablespace: 
--

CREATE TABLE miruta_parada (
    id integer NOT NULL,
    posicion integer NOT NULL,
    punto_id integer NOT NULL,
    recorrido_id integer NOT NULL
);


ALTER TABLE miruta_parada OWNER TO miruta;

--
-- Name: miruta_parada_id_seq; Type: SEQUENCE; Schema: public; Owner: miruta
--

CREATE SEQUENCE miruta_parada_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE miruta_parada_id_seq OWNER TO miruta;

--
-- Name: miruta_parada_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: miruta
--

ALTER SEQUENCE miruta_parada_id_seq OWNED BY miruta_parada.id;


--
-- Name: miruta_punto; Type: TABLE; Schema: public; Owner: miruta; Tablespace: 
--

CREATE TABLE miruta_punto (
    id integer NOT NULL,
    direccion text NOT NULL,
    geom geometry(Point,4326) NOT NULL
);


ALTER TABLE miruta_punto OWNER TO miruta;

--
-- Name: miruta_punto_id_seq; Type: SEQUENCE; Schema: public; Owner: miruta
--

CREATE SEQUENCE miruta_punto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE miruta_punto_id_seq OWNER TO miruta;

--
-- Name: miruta_punto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: miruta
--

ALTER SEQUENCE miruta_punto_id_seq OWNED BY miruta_punto.id;


--
-- Name: miruta_recorrido; Type: TABLE; Schema: public; Owner: miruta; Tablespace: 
--

CREATE TABLE miruta_recorrido (
    id integer NOT NULL,
    inicio text NOT NULL,
    final text NOT NULL,
    descripcion text NOT NULL,
    geom geometry(MultiLineString,4326),
    ruta_id integer NOT NULL,
    tipo character varying(100) NOT NULL
);


ALTER TABLE miruta_recorrido OWNER TO miruta;

--
-- Name: miruta_recorrido_id_seq; Type: SEQUENCE; Schema: public; Owner: miruta
--

CREATE SEQUENCE miruta_recorrido_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE miruta_recorrido_id_seq OWNER TO miruta;

--
-- Name: miruta_recorrido_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: miruta
--

ALTER SEQUENCE miruta_recorrido_id_seq OWNED BY miruta_recorrido.id;


--
-- Name: miruta_roles; Type: TABLE; Schema: public; Owner: miruta; Tablespace: 
--

CREATE TABLE miruta_roles (
    id integer NOT NULL,
    descripcion character varying(255)
);


ALTER TABLE miruta_roles OWNER TO miruta;

--
-- Name: miruta_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: miruta
--

CREATE SEQUENCE miruta_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE miruta_roles_id_seq OWNER TO miruta;

--
-- Name: miruta_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: miruta
--

ALTER SEQUENCE miruta_roles_id_seq OWNED BY miruta_roles.id;


--
-- Name: miruta_ruta; Type: TABLE; Schema: public; Owner: miruta; Tablespace: 
--

CREATE TABLE miruta_ruta (
    id integer NOT NULL,
    nombre character varying(255) NOT NULL,
    base_id integer not null references miruta_base (id)
);


ALTER TABLE miruta_ruta OWNER TO miruta;

--
-- Name: miruta_ruta_id_seq; Type: SEQUENCE; Schema: public; Owner: miruta
--

CREATE SEQUENCE miruta_ruta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE miruta_ruta_id_seq OWNER TO miruta;

--
-- Name: miruta_ruta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: miruta
--

ALTER SEQUENCE miruta_ruta_id_seq OWNED BY miruta_ruta.id;


--
-- Name: miruta_servicio; Type: TABLE; Schema: public; Owner: miruta; Tablespace: 
--

CREATE TABLE miruta_servicio (
    id integer NOT NULL,
    nombre character varying(255),
    descripcion character varying(255)
);


ALTER TABLE miruta_servicio OWNER TO miruta;

--
-- Name: miruta_servicio_id_seq; Type: SEQUENCE; Schema: public; Owner: miruta
--

CREATE SEQUENCE miruta_servicio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE miruta_servicio_id_seq OWNER TO miruta;

--
-- Name: miruta_servicio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: miruta
--

ALTER SEQUENCE miruta_servicio_id_seq OWNED BY miruta_servicio.id;


--
-- Name: miruta_tipo_comentario; Type: TABLE; Schema: public; Owner: miruta; Tablespace: 
--

CREATE TABLE miruta_tipo_comentario (
    idtipo integer NOT NULL,
    descripcion character varying(255)
);


ALTER TABLE miruta_tipo_comentario OWNER TO miruta;

--
-- Name: miruta_tipo_comentario_idtipo_seq; Type: SEQUENCE; Schema: public; Owner: miruta
--

CREATE SEQUENCE miruta_tipo_comentario_idtipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE miruta_tipo_comentario_idtipo_seq OWNER TO miruta;

--
-- Name: miruta_tipo_comentario_idtipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: miruta
--

ALTER SEQUENCE miruta_tipo_comentario_idtipo_seq OWNED BY miruta_tipo_comentario.idtipo;


--
-- Name: miruta_usuario; Type: TABLE; Schema: public; Owner: miruta; Tablespace: 
--

CREATE TABLE miruta_usuario (
    id integer NOT NULL,
    usuario character varying(255),
    password character varying(255),
    miruta_rolesid integer NOT NULL
);


ALTER TABLE miruta_usuario OWNER TO miruta;

--
-- Name: miruta_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: miruta
--

CREATE SEQUENCE miruta_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE miruta_usuario_id_seq OWNER TO miruta;

--
-- Name: miruta_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: miruta
--

ALTER SEQUENCE miruta_usuario_id_seq OWNED BY miruta_usuario.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_base ALTER COLUMN id SET DEFAULT nextval('miruta_base_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_grupo ALTER COLUMN id SET DEFAULT nextval('miruta_grupo_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_noticia ALTER COLUMN id SET DEFAULT nextval('miruta_noticia_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_parada ALTER COLUMN id SET DEFAULT nextval('miruta_parada_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_punto ALTER COLUMN id SET DEFAULT nextval('miruta_punto_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_recorrido ALTER COLUMN id SET DEFAULT nextval('miruta_recorrido_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_roles ALTER COLUMN id SET DEFAULT nextval('miruta_roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_ruta ALTER COLUMN id SET DEFAULT nextval('miruta_ruta_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_servicio ALTER COLUMN id SET DEFAULT nextval('miruta_servicio_id_seq'::regclass);


--
-- Name: idtipo; Type: DEFAULT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_tipo_comentario ALTER COLUMN idtipo SET DEFAULT nextval('miruta_tipo_comentario_idtipo_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_usuario ALTER COLUMN id SET DEFAULT nextval('miruta_usuario_id_seq'::regclass);


--
-- Name: miruta_base_pkey; Type: CONSTRAINT; Schema: public; Owner: miruta; Tablespace: 
--

ALTER TABLE ONLY miruta_base
    ADD CONSTRAINT miruta_base_pkey PRIMARY KEY (id);


--
-- Name: miruta_grupo_pkey; Type: CONSTRAINT; Schema: public; Owner: miruta; Tablespace: 
--

ALTER TABLE ONLY miruta_grupo
    ADD CONSTRAINT miruta_grupo_pkey PRIMARY KEY (id);


--
-- Name: miruta_noticia_pkey; Type: CONSTRAINT; Schema: public; Owner: miruta; Tablespace: 
--

ALTER TABLE ONLY miruta_noticia
    ADD CONSTRAINT miruta_noticia_pkey PRIMARY KEY (id);


--
-- Name: miruta_parada_pkey; Type: CONSTRAINT; Schema: public; Owner: miruta; Tablespace: 
--

ALTER TABLE ONLY miruta_parada
    ADD CONSTRAINT miruta_parada_pkey PRIMARY KEY (id);


--
-- Name: miruta_punto_geom_key; Type: CONSTRAINT; Schema: public; Owner: miruta; Tablespace: 
--

ALTER TABLE ONLY miruta_punto
    ADD CONSTRAINT miruta_punto_geom_key UNIQUE (geom);


--
-- Name: miruta_punto_pkey; Type: CONSTRAINT; Schema: public; Owner: miruta; Tablespace: 
--

ALTER TABLE ONLY miruta_punto
    ADD CONSTRAINT miruta_punto_pkey PRIMARY KEY (id);


--
-- Name: miruta_recorrido_pkey; Type: CONSTRAINT; Schema: public; Owner: miruta; Tablespace: 
--

ALTER TABLE ONLY miruta_recorrido
    ADD CONSTRAINT miruta_recorrido_pkey PRIMARY KEY (id);


--
-- Name: miruta_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: miruta; Tablespace: 
--

ALTER TABLE ONLY miruta_roles
    ADD CONSTRAINT miruta_roles_pkey PRIMARY KEY (id);


--
-- Name: miruta_ruta_nombre_key; Type: CONSTRAINT; Schema: public; Owner: miruta; Tablespace: 
--

ALTER TABLE ONLY miruta_ruta
    ADD CONSTRAINT miruta_ruta_nombre_key UNIQUE (nombre);


--
-- Name: miruta_ruta_pkey; Type: CONSTRAINT; Schema: public; Owner: miruta; Tablespace: 
--

ALTER TABLE ONLY miruta_ruta
    ADD CONSTRAINT miruta_ruta_pkey PRIMARY KEY (id);


--
-- Name: miruta_servicio_pkey; Type: CONSTRAINT; Schema: public; Owner: miruta; Tablespace: 
--

ALTER TABLE ONLY miruta_servicio
    ADD CONSTRAINT miruta_servicio_pkey PRIMARY KEY (id);


--
-- Name: miruta_tipo_comentario_pkey; Type: CONSTRAINT; Schema: public; Owner: miruta; Tablespace: 
--

ALTER TABLE ONLY miruta_tipo_comentario
    ADD CONSTRAINT miruta_tipo_comentario_pkey PRIMARY KEY (idtipo);


--
-- Name: miruta_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: miruta; Tablespace: 
--

ALTER TABLE ONLY miruta_usuario
    ADD CONSTRAINT miruta_usuario_pkey PRIMARY KEY (id);


--
-- Name: miruta_operador_nombre_a1c6607f71f6a3b_like; Type: INDEX; Schema: public; Owner: miruta; Tablespace: 
--

CREATE INDEX miruta_operador_nombre_a1c6607f71f6a3b_like ON miruta_base USING btree (nombre);


--
-- Name: miruta_operador_nombre_key; Type: INDEX; Schema: public; Owner: miruta; Tablespace: 
--

CREATE UNIQUE INDEX miruta_operador_nombre_key ON miruta_base USING btree (nombre);


--
-- Name: miruta_parada_6a30947b; Type: INDEX; Schema: public; Owner: miruta; Tablespace: 
--

CREATE INDEX miruta_parada_6a30947b ON miruta_parada USING btree (punto_id);


--
-- Name: miruta_parada_cf3933d2; Type: INDEX; Schema: public; Owner: miruta; Tablespace: 
--

CREATE INDEX miruta_parada_cf3933d2 ON miruta_parada USING btree (recorrido_id);


--
-- Name: miruta_punto_geom_id; Type: INDEX; Schema: public; Owner: miruta; Tablespace: 
--

CREATE INDEX miruta_punto_geom_id ON miruta_punto USING gist (geom);


--
-- Name: miruta_recorrido_6c08ebe1; Type: INDEX; Schema: public; Owner: miruta; Tablespace: 
--

CREATE INDEX miruta_recorrido_6c08ebe1 ON miruta_recorrido USING btree (ruta_id);


--
-- Name: miruta_recorrido_geom_id; Type: INDEX; Schema: public; Owner: miruta; Tablespace: 
--

CREATE INDEX miruta_recorrido_geom_id ON miruta_recorrido USING gist (geom);


--
-- Name: miruta_ruta_nombre_222bb48e2f70955e_like; Type: INDEX; Schema: public; Owner: miruta; Tablespace: 
--

CREATE INDEX miruta_ruta_nombre_222bb48e2f70955e_like ON miruta_ruta USING btree (nombre varchar_pattern_ops);


--
-- Name: miruta_par_recorrido_id_4f9939aaa047e46f_fk_miruta_recorrido_id; Type: FK CONSTRAINT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_parada
    ADD CONSTRAINT miruta_par_recorrido_id_4f9939aaa047e46f_fk_miruta_recorrido_id FOREIGN KEY (recorrido_id) REFERENCES miruta_recorrido(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: miruta_parada_punto_id_75a560901f6c5200_fk_miruta_punto_id; Type: FK CONSTRAINT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_parada
    ADD CONSTRAINT miruta_parada_punto_id_75a560901f6c5200_fk_miruta_punto_id FOREIGN KEY (punto_id) REFERENCES miruta_punto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: miruta_recorrido_ruta_id_74d0b4de6b015bec_fk_miruta_ruta_id; Type: FK CONSTRAINT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_recorrido
    ADD CONSTRAINT miruta_recorrido_ruta_id_74d0b4de6b015bec_fk_miruta_ruta_id FOREIGN KEY (ruta_id) REFERENCES miruta_ruta(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: miruta_usuario_miruta_rolesid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: miruta
--

ALTER TABLE ONLY miruta_usuario
    ADD CONSTRAINT miruta_usuario_miruta_rolesid_fkey FOREIGN KEY (miruta_rolesid) REFERENCES miruta_roles(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

